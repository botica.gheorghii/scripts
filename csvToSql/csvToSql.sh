#/bin/bash

_FILE=$1
_TABLENAME=$2
_DATABASE=$3

echo "INSERT INTO $_DATABASE.$_TABLENAME (`cat $_FILE |head -n1`) VALUES"

_CURRLINE=2
_TOTALLINES=`cat $_FILE |wc -l`

sed 1d $_FILE | while read p; do
    _COLUMNSCOUNT=`echo "($p)" | awk -F"," '{print NF-1}'`
    if [ $_CURRLINE -eq $_TOTALLINES ]
    then
        echo -n "(\""$p | sed -r 's/,/","/g'
        echo "\");"
        _CURRLINE=$(($_CURRLINE+1))
    else
        echo -n "(\""$p | sed -r 's/,/","/g'
        echo "\"),"
        _CURRLINE=$(($_CURRLINE+1))
    fi
done
